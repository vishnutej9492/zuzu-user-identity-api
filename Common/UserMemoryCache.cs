﻿using Microsoft.Extensions.Caching.Memory;

namespace zuzu_user_identity_api.Common
{
    public class UserMemoryCache
    {
        public MemoryCache Cache { get; set; }
        public UserMemoryCache()
        {
            Cache = new MemoryCache(new MemoryCacheOptions
            {
                SizeLimit = 1024
            });
        }
    }
}