﻿using System;

namespace zuzu_user_identity_api.Common
{
    public static class TimeZoneUtil
    {
        public static TimeZoneInfo IndianTimeZone = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
        public static DateTime GetIndianCurrentDateTime(DateTime dateTime)
        {
            return TimeZoneInfo.ConvertTimeFromUtc(dateTime, IndianTimeZone);
        }
    }
}