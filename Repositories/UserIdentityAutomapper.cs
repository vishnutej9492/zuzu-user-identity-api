﻿using System;
using AutoMapper;
using zuzu_user_identity_api.Models;

namespace zuzu_user_identity_api.Repositories
{
    public class UserIdentityAutomapper: Profile
    {
        public UserIdentityAutomapper()
        {
            CreateMap<UserIdentity, UserIdentityEntity>();
            CreateMap<UserIdentityEntity, UserIdentity>();
            // Mapper for user identity updates
            // Condition will replace the value only
            // if the property is not null or not of type DateTime
            CreateMap<UserIdentityEntity, UserIdentityEntity>()
                .ForAllMembers(opts => opts.Condition((src, dest, srcMember) => 
                    srcMember != null && !(srcMember is DateTime)));
        }
    }
}
