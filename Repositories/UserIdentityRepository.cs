﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using zuzu_user_identity_api.Common;
using zuzu_user_identity_api.Models;

namespace zuzu_user_identity_api.Repositories
{
    public class UserIdentityRepository
    {
        private readonly IMongoCollection<UserIdentityEntity> _info;
        private const string UserPhoneNumberString = "phoneNumber";
        public UserIdentityRepository(IZuzuUserIdentityDatabaseSettings settings)
        {
            if (settings is null)
                throw new ArgumentNullException(nameof(settings));
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);
            _info = database.GetCollection<UserIdentityEntity>(settings.CollectionName);
        }

        public async Task<bool> DoesUserExist(string userPhoneNumber)
        {
            var entity = await _info.Find(x => x.PhoneNumber == userPhoneNumber)
                .FirstOrDefaultAsync().ConfigureAwait(false);
            return entity!=null;
        }

        public async Task<UserIdentityEntity> GetUserIdentity(string phoneNumber)
        {
            return await _info.Find(x => x.PhoneNumber == phoneNumber).FirstOrDefaultAsync()
                .ConfigureAwait(false);
        }

        public async void InsertInfo(UserIdentityEntity userIdentityEntity)
        {
            if (userIdentityEntity is null)
                throw new ArgumentNullException(nameof(userIdentityEntity));
            userIdentityEntity.CreatedAt = DateTime.UtcNow;
            userIdentityEntity.ModifiedAt = DateTime.UtcNow;
            userIdentityEntity.LastLoginAt = DateTime.UtcNow;
            await _info.InsertOneAsync(userIdentityEntity).ConfigureAwait(false);
        }

        public async void UpdateInfo(string phoneNumber, UserIdentityEntity userIdentityEntity)
        {
            if (userIdentityEntity is null)
                throw new ArgumentNullException(nameof(userIdentityEntity));
            userIdentityEntity.ModifiedAt = DateTime.UtcNow;
            userIdentityEntity.LastLoginAt = DateTime.UtcNow;
            var filter = Builders<UserIdentityEntity>.Filter.Eq(UserPhoneNumberString, phoneNumber);
            await _info.ReplaceOneAsync(filter,userIdentityEntity).ConfigureAwait(false);
        }
    }
}
