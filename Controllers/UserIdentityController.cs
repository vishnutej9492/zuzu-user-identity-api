﻿using zuzu_user_identity_api.Models;
using zuzu_user_identity_api.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;

namespace zuzu_user_identity_api.Controllers
{
    /// <summary>
    /// Foo controller.
    /// </summary>
    [Route("api/[controller]")]
    public class UserIdentityController : ControllerBase
    {
        private readonly IUserIdentityService _service;
        private readonly ILogger _logger;
        private IMemoryCache _memoryCache;
        /// <summary>
        /// Creates new instance of <see cref="UserIdentityController"/>.
        /// </summary>
        /// <param name="service">Instance of <see cref="IUserIdentityService"/></param>
        /// <param name="logger"></param>
        public UserIdentityController(IUserIdentityService service, IMemoryCache cache, ILogger<UserIdentityController> logger)
        {
            _service = service ?? throw new ArgumentNullException(nameof(service));
            _memoryCache = cache ?? throw new ArgumentNullException(nameof(cache));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        /// <summary>
        /// Tries to create a new User.
        /// </summary>
        /// <param name="user">Instance of <see cref="UserIdentity"/>.</param>
        /// <response code="200">Foo created.</response>
        /// <response code="500">Internal server error.</response>
        [HttpPost("CreateUser")]
        [ProducesResponseType(typeof(int), 201)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> CreateUserIdentity([FromBody] UserIdentity user)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid request");
            if (user is null)
                throw new ArgumentNullException(nameof(user));

            var doesUserExist = user.PhoneNumber != null && await _service.DoesUserExist(user.PhoneNumber).ConfigureAwait(false);
            if (doesUserExist)
            {
                return BadRequest("User already exists, did not create");
            }
            _service.InsertUserIdentity(user);
            return Ok("User Created");
        }

        [HttpPost("UpdateUser")]
        [ProducesResponseType(typeof(int), 201)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> UpdateUserIdentity(string userPhoneNumber, [FromBody] UserIdentity user)
        {
            if (!ModelState.IsValid)
                return BadRequest(JsonConvert.SerializeObject(new SerializableError(ModelState)));

            var outputMessage = await _service.UpdateUserIdentity(userPhoneNumber, user).ConfigureAwait(false);
            return Ok(outputMessage);
        }
        
        [HttpGet("GetUser")]
        [ProducesResponseType(typeof(int), 201)]
        [ProducesResponseType(500)]
        public async Task<IActionResult> GetUserIdentity(string userPhoneNumber)
        {
            if (string.IsNullOrEmpty(userPhoneNumber))
                return BadRequest("Invalid request");
            try
            {
                var outputMessage = await _service.GetUserIdentity(userPhoneNumber).ConfigureAwait(false);
                return Ok(outputMessage);
            }
            catch(ArgumentException e)
            {
                return BadRequest(e);
            }
        }
    }
}