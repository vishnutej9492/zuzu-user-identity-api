﻿using System.Collections.Generic;
using System.Threading.Tasks;
using zuzu_user_identity_api.Models;

namespace zuzu_user_identity_api.Services
{
    /// <summary>
    /// Represents the set of methods for Foo manipulation.
    /// </summary>
    public interface IUserIdentityService
    {
        /// <summary>
        /// Tries to create new Foo.
        /// </summary>
        /// <param name="phoneNumber">Instance of <see cref="UserIdentity"/></param>
        /// <returns>Unique identifier.</returns>
        public Task<bool> DoesUserExist(string phoneNumber);
        public void InsertUserIdentity(UserIdentity userIdentity);
        public Task<string> UpdateUserIdentity(string userPhoneNumber, UserIdentity userIdentity);
        public Task<UserIdentity> GetUserIdentity(string userPhoneNumber);
    }
}