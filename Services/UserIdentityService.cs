﻿using zuzu_user_identity_api.Models;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using zuzu_user_identity_api.Common;
using zuzu_user_identity_api.Repositories;

namespace zuzu_user_identity_api.Services
{
    public sealed class UserIdentityService : IUserIdentityService
    {
        private readonly UserIdentityRepository _userIdentityRepository;
        private readonly IMapper _mapper;
        private readonly ILogger<UserIdentityService> _logger;
        private MemoryCache _cache;
        private const string UserDoesNotExistException = "User does not exist";
        public UserIdentityService(UserIdentityRepository userIdentityRepository, UserMemoryCache memoryCache,
            IMapper mapper, ILogger<UserIdentityService> logger)
        {
            _userIdentityRepository = userIdentityRepository;
            _cache = memoryCache.Cache;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<bool> DoesUserExist(string userPhoneNumber)
        {
            // Check if it exists in cache and then go to the repository.
            return _cache.TryGetValue(userPhoneNumber, out UserIdentity resultIdentity) ||
                   await _userIdentityRepository.DoesUserExist(userPhoneNumber).ConfigureAwait(false);
        }

        public async Task<string> UpdateUserIdentity(string userPhoneNumber, UserIdentity updatedUserIdentity)
        {
            if (string.IsNullOrEmpty(userPhoneNumber))
                return "Phone number or information was invalid";
            var doesPartnerExist = await DoesUserExist(userPhoneNumber).ConfigureAwait(false);
            if (!doesPartnerExist)
                return "User does not exist";
            var updatedInfoEntity = _mapper.Map<UserIdentityEntity>(updatedUserIdentity);
            var oldInfoEntity = await _userIdentityRepository.GetUserIdentity(userPhoneNumber).ConfigureAwait(false);
            _mapper.Map(updatedInfoEntity, oldInfoEntity);
            _userIdentityRepository.UpdateInfo(userPhoneNumber, oldInfoEntity);
            var completeUserIdentity = _mapper.Map<UserIdentity>(oldInfoEntity);

            _cache.Remove(userPhoneNumber);
            await _cache.GetOrCreateAsync(userPhoneNumber, entry =>
            {
                entry.SlidingExpiration = TimeSpan.FromHours(4);
                return Task.FromResult(completeUserIdentity);
            }).ConfigureAwait(false);
            return "User updated";
        }

        public async Task<UserIdentity> GetUserIdentity(string userPhoneNumber)
        {
            // Check if phone number is in cache.
            if (_cache.TryGetValue(userPhoneNumber, out UserIdentity userIdentityFromCache))
                return userIdentityFromCache;

            var doesPartnerExist = await DoesUserExist(userPhoneNumber).ConfigureAwait(false);
            if (!doesPartnerExist)
                throw new ArgumentNullException(nameof(userPhoneNumber), UserDoesNotExistException);
            var userIdentityEntity = await _userIdentityRepository.GetUserIdentity(userPhoneNumber).ConfigureAwait(false);
            var userIdentity = _mapper.Map<UserIdentity>(userIdentityEntity);

            // Set entry in cache and return user identity.
            return await _cache.GetOrCreateAsync(userPhoneNumber, entry =>
            {
                entry.SlidingExpiration = TimeSpan.FromHours(4);
                entry.Size = 32;
                return Task.FromResult(userIdentity);
            }).ConfigureAwait(false);
        }

        public async void InsertUserIdentity(UserIdentity userIdentity)
        {
            if (userIdentity is null)
                throw new ArgumentNullException(nameof(userIdentity));
            userIdentity.UserGuid = Guid.NewGuid();
            await _cache.GetOrCreateAsync(userIdentity.PhoneNumber, entry =>
            {
                entry.SlidingExpiration = TimeSpan.FromHours(4);
                return Task.FromResult(userIdentity);
            }).ConfigureAwait(false);
            var customerInfoEntity = _mapper.Map<UserIdentityEntity>(userIdentity);
            _userIdentityRepository.InsertInfo(customerInfoEntity);
        }
    }
}