﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace zuzu_user_identity_api.Models
{
    /// <summary>
    ///     Partner model
    /// </summary>
    public class UserIdentityEntity
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string? Id { get; set; }
        /// <summary>
        /// Gets or sets partner unique identifier.
        /// </summary>
        [BsonElement("userGuid")]
        public Guid UserGuid { get; set; }
        /// <summary>
        /// Gets or sets partner id number.
        /// </summary>
        [BsonElement("firebaseUid")]
        public string? FirebaseUid { get; set; }
        /// <summary>
        /// Gets or sets the First Name.
        /// </summary>
        [BsonElement("firstName")]
        public string? FirstName { get; set; }
        /// <summary>
        /// Gets or sets the Last Name.
        /// </summary>
        [BsonElement("lastName")]
        public string? LastName { get; set; }
        /// <summary>
        /// Gets or sets the Phone Number.
        /// </summary>
        [BsonElement("phoneNumber")]
        public string? PhoneNumber { get; set; }
        /// <summary>
        /// Gets or sets the Email Address.
        /// </summary>
        [BsonElement("emailAddress")]
        public string? EmailAddress { get; set; }
        [BsonElement("lastLoginAt")]
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime LastLoginAt { get; set; }
        [BsonElement("createdAt")]
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime CreatedAt { get; set; }
        [BsonElement("modifiedAt")]
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime ModifiedAt { get; set; }
    }
}