﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace zuzu_user_identity_api.Models
{
    /// <summary>
    ///     Partner model
    /// </summary>
    public class UserIdentity
    {
        /// <summary>
        /// Gets or sets partner unique identifier.
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please Valid Guid")]
        public Guid UserGuid { get; set; }
        /// <summary>
        /// Gets or sets firebase uid.
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please Firebase Uid")]
        [JsonProperty(PropertyName = "firebaseUid")]
        public string? FirebaseUid { get; set; }
        /// <summary>
        /// Gets or sets the First Name.
        /// </summary>
        //[Required(AllowEmptyStrings = false, ErrorMessage = "Please Provide First Name")]
        [JsonProperty(PropertyName = "firstName")]
        public string? FirstName { get; set; }
        /// <summary>
        /// Gets or sets the Last Name.
        /// </summary>
        //[Required(AllowEmptyStrings = false, ErrorMessage = "Please Provide Last Name")]
        [JsonProperty(PropertyName = "lastName")]
        public string? LastName { get; set; }
        /// <summary>
        /// Gets or sets the Phone Number.
        /// </summary>
        //[Required(AllowEmptyStrings = false, ErrorMessage = "Please Provide Phone Number")]
        [StringLength(10, MinimumLength = 10, ErrorMessage = "Phone number is not valid")]
        [Phone(ErrorMessage = "Phone number is not valid")]
        [JsonProperty(PropertyName = "phoneNumber")]
        public string? PhoneNumber { get; set; }
        /// <summary>
        /// Gets or sets the Email Address.
        /// </summary>
        //[Required(AllowEmptyStrings = false, ErrorMessage = "Please Provide Email Address")]
        [JsonProperty(PropertyName = "emailAddress")]
        public string? EmailAddress { get; set; }
        [JsonProperty("lastLoginAt")]
        public DateTime LastLoginAt { get; set; }
    }
}